import React, { useState } from 'react';
import Error from './Error';

const Formulario = ({ setSearch }) => {
  const [termino, setTermino] = useState('');
  const [error, setError] = useState(false);
  const handleChange = (e) => {
    setTermino(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    if (termino.trim() === '') {
      setError(true);
      return null;
    }
    setError(false);
    setSearch(termino);
  };

  return (
    <form onSubmit={handleSubmit}>
      {error && <Error mensaje='El campo no debe estar vacio' />}
      <div className='row'>
        <div className='form-group col-md-9'>
          <input
            type='text'
            className='form-control form-control-lg'
            placeholder='Busca una imagen'
            onChange={handleChange}
          />
        </div>
        <div className='form-group col-md-3'>
          <input
            type='submit'
            className='btn btn-lg btn-success btn-block'
            value={'Buscar'}
          />
        </div>
      </div>
    </form>
  );
};

export default Formulario;
