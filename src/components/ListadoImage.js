import React from 'react';
import Imagenes from './Imagenes';

const ListadoImage = ({ images }) => {
  return (
    <div className='col-12 p-5 row'>
      {images.map((imagen) => {
        return <Imagenes key={imagen.id} imagen={imagen} />;
      })}
    </div>
  );
};

export default ListadoImage;
