import { useEffect, useState } from 'react';
import Formulario from './components/Formulario';
import ListadoImage from './components/ListadoImage';

function App() {
  const [search, setSearch] = useState('');
  const [images, setImages] = useState([]);
  const [currenPage, setCurrenPage] = useState(1);
  const [pagesTotal, setPagesTotal] = useState(1);
  useEffect(() => {
    if (search === '') return;

    const imagePagine = 30;
    const KEY = '19297485-beb00959478a231d7de395af4';
    const consultarAPI = async () => {
      const url = `https://pixabay.com/api/?key=${KEY}&q=${search}&per_page=${imagePagine}&page=${currenPage}`;
      const resultado = await fetch(url);
      const data = await resultado.json();
      setImages(data.hits);
      // paginacion
      const calcularPaginas = Math.ceil(data.totalHits / imagePagine);
      setPagesTotal(calcularPaginas);
      // mover al inicio
      const jumbotron = document.querySelector('.jumbotron');
      jumbotron.scrollIntoView({ behavior: 'smooth' });
    };
    consultarAPI();
  }, [search, currenPage]);

  const handleAnterior = () => {
    const nuevaPagina = currenPage - 1;
    if (nuevaPagina === 0) return;
    setCurrenPage(nuevaPagina);
  };
  const handleSiguiente = () => {
    const nuevaPagina = currenPage + 1;
    if (nuevaPagina > pagesTotal) return;
    setCurrenPage(nuevaPagina);
  };
  return (
    <div className='container mb-4'>
      <div className='jumbotron'>
        <p className='lead text-center'>Buscador de imagenes</p>
        <Formulario setSearch={setSearch} />
      </div>
      <div className='row justify-content-center'>
        <ListadoImage images={images} />
        {currenPage === 1 ? null : (
          <button
            type='button'
            className='btn btn-info mr-1'
            onClick={handleAnterior}
          >
            &laquo; Anterior
          </button>
        )}
        {currenPage === pagesTotal ? null : (
          <button
            type='button'
            className='btn btn-info mr-1'
            onClick={handleSiguiente}
          >
            Siguiente &raquo;{' '}
          </button>
        )}
      </div>
    </div>
  );
}

export default App;
